# OnTheBeach

## Problem Statement

Imagine we have a list of jobs, each represented by a character. Because certain jobs must be done before others, a job may have a
dependency on another job. For example, a may depend on b, meaning the final sequence of jobs should place b before a. If a has no
dependency, the position of a in the final sequence does not matter.

Given you’re passed an empty string (no jobs), the result should be an empty sequence.  

Given the following job structure:  
>a =>

The result should be a sequence consisting of a single job a.  

Given the following job structure:  
>a =>  
b =>  
c =>

The result should be a sequence containing all three jobs abc in no significant order.  

Given the following job structure:  
>a =>  
b => c  
c =>

The result should be a sequence that positions c before b, containing all three jobs abc.  

Given the following job structure:  
>a =>  
b => c  
c => f  
d => a  
e => b  
f =>

The result should be a sequence that positions f before c, c before b, b before e and a before d containing all six jobs abcdef.  

Given the following job structure:  
>a =>  
b =>  
c => c

The result should be an error stating that jobs can’t depend on themselves.  

Given the following job structure:  
>a =>  
b => c  
c => f  
d => a  
e =>  
f => b

The result should be an error stating that jobs can’t have circular dependencies.  


## Solution
I started by writing a test for each of the given structures to match the desired expectations.  
I wrote a ruby class called `JobRunner` that get initialised with the structures string argument, followed by a `.run` method  
> One of the quick thinking I did was that the jobs without dependency should run first before the ones with dependency

  **.run method**  
  This method does 2 things:  
  - runs the method called `sequence_jobs` that checks and resolves dependency  
  - returns the result in string  

  **.sequence_jobs method**  
  This method does the following things:  
  - Separate the structures without dependency and extracts their jobs  
  - loops through the structure with dependencies and passes them to the `.resolve_dependency` method  

  **resolve_dependency(structure) method**  
  Method accepts a structure as an argument.  
  This method does the following things:  
  - extracts the job and the job it's depending on.  
  - checks for self dependency error  
  - checks for circular dependency error  
  - and resolves dependency  (i.e the other job(b) that job(a) is depending on, get to run first(b) before the job(a))

**Self dependency**  
The program would raise a self dependency error if structure contains job that depends on itself  

**Circular dependency**  
The program would raise a circular dependency error if structure has depends on each other like this: job(b) depends on job(c) and job(c) depends on job(f) and job(f) also depends on job (b)   
e.g. `JobRunner.new('b => c\nc => f\nf => b').run`  


## How to run program

While in the root folder of the project run:

```ruby
irb
```

```ruby
irb(main):001:0> require './job_runner.rb'
irb(main):002:0> JobRunner.new('t => o\nb => t').run
```

**NOTE:**
 - Each structure line is separated by `\n`
 - three line structure would look like this `t => o\nb => t\np => p`

## How to run program tests  
Make sure you have the rspec gem installed, the test suite was initialised using rspec --init  
While in the root folder of the project just run:  

```ruby
 rspec
```


## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/luvolugbemmy/onthebeach. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
