# frozen_string_literal: true

require 'spec_helper'
require './job_runner.rb'

RSpec.describe JobRunner do
  describe '#run' do
    let(:structure) { '' }

    context 'with empty string (no jobs)' do
      it 'should be an empty sequence' do
        expect(JobRunner.new(structure).run).to eq ''
      end
    end

    context 'with structure  (a =>)' do
      let(:structure) { 'a => ' }

      it { expect(JobRunner.new(structure).run).to eq 'a' }
    end

    context 'with structure (a => \nb =>\n c =>)' do
      let(:structure) { 'a => \nb =>\n c => ' }

      it { expect(JobRunner.new(structure).run).to eq 'abc' }
    end

    context 'with structure (a => \nb => c\n c => )' do
      let(:structure) { 'a => \nb => c\n c =>' }

      it { expect(JobRunner.new(structure).run).to eq 'acb' }
    end

    context 'with structure (a => \nb => c\nc => f\nd => a\ne => b\nf =>)' do
      let(:structure) { 'a => \nb => c\nc => f\nd => a\ne => b\nf => ' }

      it { expect(JobRunner.new(structure).run).to eq 'afcbde' }
    end

    context 'with structure (a => \nb => \nc => c)' do
      let(:structure) { 'a => \nb => \nc => c' }

      it do
        expect do
          JobRunner.new(structure).run
        end.to raise_error RuntimeError, "Jobs can't depend on themselves."
      end
    end

    context 'with structure (a => \nb => c\nc => f\nd => a\ne => \nf => b)' do
      let(:structure) { 'a => \nb => c\nc => f\nd => a\ne => \nf => b' }

      it do
        expect do
          JobRunner.new(structure).run
        end.to raise_error RuntimeError, "Jobs can't have circular dependencies"
      end
    end
  end
end
