# frozen_string_literal: true

# JobRunner class
class JobRunner
  attr_accessor :structures, :jobs

  def initialize(structure)
    @structures = structure.to_s.split('\n').reject(&:empty?)
    @jobs = []
  end

  def run
    sequence_jobs
    jobs.join('')
  end

  private

  def sequence_jobs
    no_dependencies = structures.reject { |s| dependencies?(s) }
    no_dependencies.each { |s| jobs << extract_job(s) }
    with_dependencies = structures.select { |s| dependencies?(s) }
    with_dependencies.each do |structure|
      resolve_dependency(structure)
    end
  end

  def dependencies?(structure)
    structure.match(/^[a-zA-Z]\s=>\s[a-zA-Z]/)
  end

  def extract_job(structure)
    structure.split('=>').first.strip
  end

  def extract_dependency(structure)
    return '' if structure.split('=>').reject(&:empty?).count == 1

    structure.split('=>').last.strip
  end

  def resolve_dependency(structure)
    dependency = extract_dependency(structure)
    job = extract_job(structure)
    check_dependencies(job, dependency)
    jobs << dependency unless jobs.include?(dependency)
    jobs << job if jobs.include?(dependency) && !jobs.include?(job)
  end

  def check_dependencies(job, dependency)
    raise "Jobs can't depend on themselves." if job == dependency

    msg = "jobs can't have circular dependencies"
    raise msg if jobs.include?(job) && !jobs.include?(dependency)
  end
end
